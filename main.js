export function setup(ctx) {
    const SETTINGS_GENERAL = 'General'
    const MULTIPLIER_NAME = 'go-fast-multiplier'

    // Setup the settings menu
    ctx.settings.section(SETTINGS_GENERAL).add({
        name: MULTIPLIER_NAME,
        type:'dropdown',
        label: 'Go Fast Multiplier',
        hint:'Adjusts the tick multiplier to speed the game up by the selected amount. Disabled notifications for values above 3. WARNING: VALUES ABOVE 5X HAVE NOT BEEN TESTED BY THE DEVELOPER, AND MAY CRASH YOUR GAME/BROSWER.',
        default: '1',
        onChange:(newMultiplier, oldMultiplier) => overwriteProgressBarAnimation(newMultiplier, oldMultiplier),
        options: [
            {
                value:'100',
                display:'100x'
            },
            {
                value:'50',
                display:'50x'
            },
            {
                value:'10',
                display:'10x'
            },
            {
                value:'5',
                display:'5x'
            },
            {
                value:'3',
                display:'3x'
            },
            {
                value:'2',
                display:'2x'
            },
            {
                value:'1',
                display:'1x'
            },
            {
                value:'0.5',
                display:'0.5x'
            },
            {
                value:'0.25',
                display:'0.25x'
            }
        ]
    })
    
    // Notification Management Bindings
    const _notificationAdd = game.combat.notifications.add.bind(game.combat.notifications);
    const _toastifyShowToast = Toastify.prototype.showToast;
    
    const blackholeNotifications = () => {
		game.combat.notifications.add = function(notification) {};
		Toastify.prototype.showToast = function() {} 
    }

    const restoreNotifications = () => {
		game.combat.notifications.add = _notificationAdd;
		Toastify.prototype.showToast = _toastifyShowToast;
    }

    const overwriteProgressBarAnimation = (newMultiplier, oldMultiplier) => {
        const gameSpeedModifier = parseFloat(newMultiplier)
        
        // restore notifications
        if(oldMultiplier >=5 && newMultiplier < 5) {
            restoreNotifications()
        }
       
        // black hole notifications
        if(oldMultiplier < 5 && newMultiplier >= 5) {
            blackholeNotifications()
        }

        ProgressBar.prototype.animateProgressFromTimer = function (timer) { this.animateProgress((timer.maxTicks-timer.ticksLeft) * (TICK_INTERVAL / gameSpeedModifier), timer.maxTicks * (TICK_INTERVAL / gameSpeedModifier)); }
    }

    // Game hooks
    ctx.onCharacterLoaded(ctx => {
        ctx.patch(Game, 'processTime').replace(function() {
            const gameSpeedModifier = parseFloat(ctx.settings.section(SETTINGS_GENERAL).get(MULTIPLIER_NAME))

            const currentTickTime = performance.now();
            let ticksToRun = Math.floor((currentTickTime - this.previousTickTime) / (TICK_INTERVAL / gameSpeedModifier));

            if (ticksToRun > this.maxOfflineTicks) {
                ticksToRun = this.maxOfflineTicks;
                this.previousTickTime = currentTickTime - ticksToRun * (TICK_INTERVAL / gameSpeedModifier);
            }
            this.runTicks(ticksToRun);
            this.previousTickTime += ticksToRun * (TICK_INTERVAL / gameSpeedModifier);
        })

        ctx.patch(Farming, 'getPlotGrowthTime').replace(function(oldfunc, plot) {
            const gameSpeedModifier = parseFloat(ctx.settings.section(SETTINGS_GENERAL).get(MULTIPLIER_NAME))

            const timer = this.growthTimerMap.get(plot);
            if (timer === undefined)
                return 0;
            return timer.ticksLeft * (TICK_INTERVAL / gameSpeedModifier);
        })

        overwriteProgressBarAnimation(ctx.settings.section(SETTINGS_GENERAL).get(MULTIPLIER_NAME))
    })

}
